#ifndef MAP_H
#define MAP_H
/*
 * =====================================================================================
 *
 *       Filename:  map.h
 *
 *    Description: Definitions for the map class. 
 *
 *        Version:  1.0
 *        Created:  19/10/2012 19:27:45
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include <chrono>
#include <random>
#include <vector>
const unsigned int squareSize = 15;
class Map{
    public:
        //C-tors
        Map();
        Map(const unsigned int x, const unsigned int y, const unsigned int amount);

        void set(const unsigned int x, const unsigned int y, const unsigned int value);
        unsigned int get(unsigned int x, unsigned y);
        unsigned int x() const{return xSize;};
        unsigned int y() const{return ySize;};
        int size(){return mineField.size();};
        void insertBomb(const unsigned int x, const unsigned int y);
        void fill(const unsigned int amount){
            std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
            fill(generator(), amount);
        }

    private:
        unsigned int xSize, ySize;
        std::vector<unsigned short> mineField;

        void increment(const unsigned int x, const unsigned int y);
        void fill(const unsigned int seed, const unsigned int amount);
};
#endif
