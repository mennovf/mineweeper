/*
 * =====================================================================================
 *
 *       Filename:  EventClass.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  25/10/2012 20:55:29
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include "eventClass.h"
EventClass::EventClass() : cLMB{ false }, prevMousePos{}{
}

void EventClass::handleEvent(sf::Event event){
    switch (event.type){
        case (sf::Event::Closed): handleClose(event); break;
        case (sf::Event::MouseButtonPressed): handleClick(event); break;
        case (sf::Event::MouseButtonReleased): handleRelease(event); break;
        case (sf::Event::Resized): handleResize(event); break;
        case (sf::Event::MouseEntered): handleFocus(event); break;
        case (sf::Event::MouseMoved): mouseMovedHelper(event); break;
    }
}

void EventClass::handleClick(sf::Event event){
    switch(event.mouseButton.button){
        case sf::Mouse::Left: leftClickHelper(event); break;
        case sf::Mouse::Right: handleRightClick(event); break;
        case sf::Mouse::Middle: handleMiddleClick(event); break;
    }
}

void EventClass::handleRelease(sf::Event event){
    switch(event.mouseButton.button){
        case sf::Mouse::Left: leftReleaseHelper(event); break;
        case sf::Mouse::Right: handleRightRelease(event); break;
        case sf::Mouse::Middle: handleMiddleRelease(event); break;
    }
}

void EventClass::leftClickHelper(sf::Event event){
    cLMB = true;
    prevMousePos.x = event.mouseButton.x;
    prevMousePos.y = event.mouseButton.y;
    handleLeftClick(event);
}

void EventClass::leftReleaseHelper(sf::Event event){
    cLMB = false;
    handleLeftRelease(event);
}

void EventClass::mouseMovedHelper(sf::Event event){
    if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
        handleMouseDrag(event);
        prevMousePos.x = event.mouseMove.x;
        prevMousePos.y = event.mouseMove.y;
    }
    handleMouseMove(event);
}
