#ifndef GAME_H
#define GAME_H
/*
 * =====================================================================================
 *
 *       Filename:  game.h
 *
 *    Description:  The main game class for mineweeper game
 *
 *        Version:  1.0
 *        Created:  24/10/2012 16:45:35
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include "map.h"
#include "eventClass.h"
#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>


enum class Choice : char;

class Game : public EventClass{
    public:
        Game();
        void run();
    private:
        void uncover(unsigned int x, unsigned int y);
        void toggleMark(unsigned int x, unsigned int y);
        void checkWin();

        sf::RenderWindow window;
        void render();
        void gameOver();

        bool actionHappend;
        bool running;
        Map field;
        std::vector<Choice> playerChoice;
        const int bombs;

        //Textures shouldn't be modified on their own
        std::vector<sf::Texture> textures;
        void loadTex();

        sf::Sprite getSprite(int x, int y);

        Choice getChoice(unsigned int x, unsigned int y){return playerChoice[y*field.x() + x];};
        void setChoice(unsigned int x, unsigned int y, Choice c){playerChoice[y*field.x() + x] = c;};
        void getTileXYFromPixel(int& x, int& y, const sf::Event& event){ x = event.mouseButton.x / ( squareSize + 1 ); y = event.mouseButton.y / ( squareSize + 1 ); };

        //Inherited methods from eventclass
        virtual void handleClose(sf::Event event) override;
        virtual void handleLeftClick(sf::Event event) override;
        virtual void handleRightClick(sf::Event event) override;
        virtual void handleResize(sf::Event event) override;
        virtual void handleMiddleClick(sf::Event event) override;
};
#endif
