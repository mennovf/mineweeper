#ifndef EVENTCLASS_H
#define EVENTCLASS_H
/*
 =====================================================================================
 *
 *       Filename:  EventClass.h
 *
 *    Description:  Class meant to be inherited from to handle evfents
 *
 *        Version:  1.0
 *        Created:  25/10/2012 20:53:52
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

class EventClass{
    public:
        EventClass();
        void handleEvent(sf::Event event);
    protected:
        virtual void handleClose(sf::Event event){};

        virtual void handleLeftClick(sf::Event event){};
        virtual void handleLeftRelease(sf::Event event){};

        virtual void handleRightClick(sf::Event event){};
        virtual void handleRightRelease(sf::Event event){};

        virtual void handleMiddleClick(sf::Event event){};
        virtual void handleMiddleRelease(sf::Event event){};

        virtual void handleResize(sf::Event event){};
        virtual void handleFocus(sf::Event event){};

        virtual void handleMouseMove(sf::Event event){};
        virtual void handleMouseDrag(sf::Event event){};

        void handleClick(sf::Event event);
        void handleRelease(sf::Event event);
        
        //Previous positon of the mouse relative to the window
        //where the previous click occurred
        sf::Vector2i prevMousePos;
    private:
        //State of the left mouse button at this point
        bool cLMB;
        void leftClickHelper(sf::Event event);
        void leftReleaseHelper(sf::Event event);
        void mouseMovedHelper(sf::Event event);
};
#endif
