/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  Main file for minesweeper game using SMFL,
 *                  to get comfortable with it.
 *
 *        Version:  1.0
 *        Created:  19/10/2012 19:25:16
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include "game.h"
#include <cstdio>
int main(int argc, char* argv[]){
    Game g;
    g.run(); 
    return 0;
}
