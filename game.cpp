/*
 * =====================================================================================
 *
 *       Filename:  game.cpp
 *
 *    Description:  Implementation for game class
 *
 *        Version:  1.0
 *        Created:  24/10/2012 16:47:57
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include "game.h"
#include <iostream>

//Meant to be used as indices for an array containg the corresponding sprites
//easier on the brain
enum Tex : char{
    COVERED,
    MARKED,
    BOMB = 11
};

enum class Choice : char{
    COVERED,
    UNCOVERED,
    MARKED
};

Game::Game()
    : EventClass(), running(true), field(Map()),  playerChoice(field.size(), Choice::COVERED),
    window(sf::VideoMode(640, 480), "Minesweeper", sf::Style::Close),
    actionHappend(true), bombs(100){ 
        loadTex();
        field.fill(bombs);
    }

void Game::uncover(unsigned int x, unsigned int y){
    //Parameter validity check
    if (x < 0 || x >= field.x() || y < 0 || y >= field.y()){
        return;
    }

    Choice current = getChoice(x, y);
    if( current != Choice::COVERED){
        return;
    }

    //uncover current square and check if it's a bomb before proceeding
    setChoice(x, y, Choice::UNCOVERED);
    if ( field.get(x, y) > 8 ){
        gameOver();
        return;
    }

    //uncover the neighbouring squares if this square is an empty one
    if( field.get(x, y) == 0 ){
        for(int i = -1; i<=1; i+=2){
            uncover(x+i, y);
            uncover(x, y+i);
        }
    }
    //if this square isn't empty, return without uncovering the neighbours, added for clarity
    else{
        return;
    }
}

void Game::toggleMark(unsigned int x, unsigned int y){
    //Parameter validity check
    if (x < 0 || x >= field.x() || y < 0 || y >= field.y()){
        return;
    }
    if (getChoice(x, y) == Choice::MARKED) {
        setChoice(x, y, Choice::COVERED);
    }
    else if (getChoice(x, y) == Choice::COVERED){
        setChoice(x, y, Choice::MARKED);
    }
    return;
}

void Game::gameOver(){
    for (int x = 0; x < field.x(); x++) {
        for (int y = 0; y < field.y(); y++){
            if (field.get(x, y) > 8){
                setChoice(x, y, Choice::UNCOVERED);
                render();
            }
        }
    }
    sf::sleep(sf::seconds(2.0f));
    window.close();
    return;
}

void Game::checkWin(){
    for (int x = 0; x < field.x(); x++) {
        for (int y = 0; y < field.y(); y++){
            if (getChoice(x, y) == Choice::COVERED) {
                return;
            }
            if (getChoice(x, y) == Choice::MARKED){
                if (field.get(x, y) <= 8) {
                    return;
                }
            }
        }
    }
    gameOver();
}

sf::Sprite Game::getSprite(int x, int y){
    Choice currentChoice = getChoice(x, y);
    if (currentChoice == Choice::MARKED) {
        return sf::Sprite(textures[Tex::MARKED]);
    }
    if (currentChoice == Choice::COVERED) {
        return sf::Sprite(textures[Tex::COVERED]);
    }
    if (currentChoice == Choice::UNCOVERED) {
        int bombCount = field.get(x, y);
        if (bombCount > 8) {
            return sf::Sprite(textures[Tex::BOMB]);
        }
        return sf::Sprite(textures[bombCount+2]);
    }
    return sf::Sprite();
}

void Game::render(){
    window.clear();

    //Go  through the tiles, check if they're covered or not
    //render according to tiletype
    for (unsigned int x = 0; x < field.x(); x++){
        for (unsigned int y = 0; y < field.y(); y++){
            sf::Sprite sprite= getSprite(x, y);
            sprite.setPosition(x*(squareSize+1), y*(squareSize+1));
            window.draw(sprite);
        }
    }

    window.display();
}


void Game::run(){
    while(window.isOpen()){
        sf::Event event;
        while(window.pollEvent(event)){
            handleEvent(event);
        }
        if ( actionHappend ){
            render();
            //don't rerender unless something has changed
            actionHappend = false;
        }
    }    
}

void Game::loadTex(){
    //Empty the resources
    textures.clear();

    std::vector<std::string> resses = {"res/Tile.jpg", 
    "res/Marked.jpg", "res/0.jpg", "res/1.jpg", "res/2.jpg", "res/3.jpg",
    "res/4.jpg", "res/5.jpg", "res/6.jpg", "res/7.jpg", "res/8.jpg",
    "res/Bomb.jpg"};   
    for (auto res : resses){
        sf::Texture tex;
        tex.loadFromFile(res);
        textures.push_back(tex);
    } 
};

void Game::handleClose(sf::Event event){
    window.close();
}

void Game::handleLeftClick(sf::Event event){
    //Should both be ints (integer division)
    int x=0, y=0;
    getTileXYFromPixel(x, y, event);
    uncover(x, y);
    checkWin();
    actionHappend = true;
}
void Game::handleRightClick(sf::Event event) {
    int x=0, y=0;
    getTileXYFromPixel(x, y, event);
    toggleMark(x, y);
    checkWin();
    actionHappend=true;
}

void Game::handleResize(sf::Event event){
    actionHappend = true;   
}
void Game::handleMiddleClick(sf::Event event){
}
