/* end of include guard: MAP_H */
/*
 * =====================================================================================
 *
 *       Filename:  map.cpp
 *
 *    Description:  Implemantation for the map.
 *
 *        Version:  1.0
 *        Created:  19/10/2012 20:26:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include "map.h"
#include <cstdio>
#include <random>
#include <chrono>
#include <functional>
#include <iostream>

Map::Map() 
    : xSize(640/(squareSize+1)), ySize(480/(squareSize+1)), mineField(xSize*ySize){
    const unsigned int mines = 10;
    Map(xSize, ySize, mines);
}

Map::Map(const unsigned int x, const unsigned int y, const unsigned int amount) 
    : mineField(x*y, 0), xSize(x), ySize(y){
}

void Map::fill(const unsigned int seed, const unsigned int amount){
    //Create random number generator with time as seed
    std::mt19937 randX(seed);
    std::mt19937 randY(seed+1);
    auto xEngine = std::bind(std::uniform_int_distribution<unsigned int>(0, xSize-1), randX);
    auto yEngine = std::bind(std::uniform_int_distribution<unsigned int>(0, ySize-1), randY);
    
    //Generate a random number between 0 and x*y to fill the mineField vector with mines
    //and the nearing squares will be incremented by one
    int a = 0;
    while (a<amount){
        const unsigned int x = xEngine(), y = yEngine();
        //A square has a maximum of 8 neigbours so if the square has a value above 8, it's a bomb
        if (get(x, y) > 8){
            continue;
        }
        insertBomb(x, y);
        a++;
    }
    return;
}

unsigned int Map::get(unsigned int x, unsigned y){
    return mineField[y*xSize + x];
}

void Map::set(const unsigned int x, const unsigned int y, const unsigned int value){
    if (x >= xSize || y >= ySize || x < 0 || y < 0){
        return;
    }
    mineField[y*xSize + x] = value;
}
void Map::increment(const unsigned int x, const unsigned int y){
    if (x >= xSize || y >= ySize || x < 0 || y < 0){
        return;
    }
    mineField[y*xSize + x]++;
}
void Map::insertBomb(const unsigned int x, const unsigned int y){
    set(x, y, 9);//Will be 9 when it's incremented in the new loop
    //update the neigbouring squares
    for (short i=-1; i<=1; i++){
        for (short j=-1; j<=1; j++){
            increment(int(x)+i, int(y)+j);
        }
    }
}
